package main

import (
	"bufio"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"log"
	"net"
	"net/http"
	"strconv"
	"time"

	//"github.com/go-ini/ini"
	"github.com/gorilla/mux"
	"gitlab.com/pav_s/escpos"
	"golang.org/x/text/encoding/charmap"
)

var (
	printer_ip   string
	printer_port string
)

type JsonRPC struct {
	Jsonrpc string
	Method  string
	Params  []string
}

type CartItem struct {
	Name     string
	Qty      string
	Price    string
	Discount string
}

func main() {
	log.Fatal(run())
}

func run() error {
	//	//Получение настроек из ini файла
	//	ini, err := ini.Load("settings.ini")
	//	if err != nil {
	//		log.Println("Error loading settings file")
	//		return err
	//	}
	//	printer_ip, err := ini.Section("").GetKey("PRINTER_IP")

	//	if err == nil {
	//		log.Println("Can`t find parameter : PRINTER_IP")
	//		return err
	//	}
	//	printer_port, err := ini.Section("").GetKey("PRINTER_PORT")
	//	if err == nil {
	//		log.Println("Can`t find parameter : PRINTER_PORT")
	//		return err
	//	}

	mux := makeMuxRouter()
	httpAddr := "7777"
	log.Println("Listening on ", httpAddr)

	s := &http.Server{
		Addr:           ":" + httpAddr,
		Handler:        mux,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	if err := s.ListenAndServe(); err != nil {
		return err
	}

	return nil
}

func makeMuxRouter() http.Handler {
	muxRouter := mux.NewRouter()
	muxRouter.HandleFunc("/printer", handleRouting).Methods("POST")
	return muxRouter
}

func handleRouting(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	var request JsonRPC

	if err := decoder.Decode(&request); err != nil {
		respondWithJSON(w, r, http.StatusBadRequest, r.Body)
		return
	}

	if request.Method == "print" {
		if request.Params[2] == "true" {
			printReceipt(request.Params[0], request.Params[1], true, str2float(request.Params[3]), request.Params[4], request.Params[5])
		} else {
			printReceipt(request.Params[0], request.Params[1], false, str2float(request.Params[3]), request.Params[4], request.Params[5])
		}
		respondWithJSON(w, r, http.StatusOK, r.Body)
	}

}

func respondWithJSON(w http.ResponseWriter, r *http.Request, code int, payload interface{}) {

	response, err := json.MarshalIndent(payload, "", "  ")

	if err != nil {
		w.Header().Set("Content-type", "application/json")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("HTTP 500: Internal Server Error"))
		return
	}

	w.Header().Set("Content-type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Origin", "*")
	w.WriteHeader(200)
	w.Write(response)
}

func printReceipt(data_str string, receipt_no string, openCashDrawer bool, ext_total float64, phone string, web string) {

	socket, err := net.Dial("tcp", "192.168.0.31:9100")
	if err != nil {
		println(err.Error())
	}
	defer socket.Close()

	w := bufio.NewWriter(socket)
	p := escpos.New(w)

	p.Verbose = true

	p.Init()
	p.SetCodePageRus()
	p.SetFontSize(1, 1)
	p.SetFont("A")

	p.SetAlign("left")
	p.Logo()
	p.Linefeed()

	//	p.Write(encode866("Iris Burbonov"))
	//	p.Linefeed()
	//	p.Write(encode866("Цветочная компания"))
	//	p.Linefeed()
	p.SetAlign("center")
	p.Write(encode866("Чек № " + encode866(receipt_no)))
	p.Linefeed()
	p.Write(encode866("------------------------------------------------")) //48

	p.Linefeed()

	//Товары

	json_data, err := base64.StdEncoding.DecodeString(data_str)
	if err != nil {
		fmt.Println(err)
		return
	}

	var cart []CartItem
	err = json.Unmarshal([]byte(json_data), &cart)
	if err != nil {
		fmt.Println("error reading json:", err)
	}

	p.SetAlign("left")
	var qty, price, sum, discount, total, total_discount float64

	for i := 0; i < len(cart); i++ {
		qty = str2float(cart[i].Qty)
		price = str2float(cart[i].Price)
		discount = str2float(cart[i].Discount)
		sum = qty * price

		Name, err := base64.StdEncoding.DecodeString(cart[i].Name)
		if err != nil {
			fmt.Println(err)
			return
		}

		p.Write(string(Name))
		//p.Write(string(cart[i].Name))

		p.Linefeed()

		//Строка с количеством, ценой и суммой

		var _sumstr = "            " + float2str(sum)
		_sumstr = "=" + _sumstr[len(_sumstr)-12:]

		var __sumstr = "    " + float2str(qty) + " * " + float2str(price) + "                                              "

		__sumstr = __sumstr[0:48-len(_sumstr)] + _sumstr

		p.Write(__sumstr)
		//p.Write("    " + float2str(qty) + " * " + float2str(price) + "                       =  1500000.00")
		p.Linefeed()

		//Строка со скидкой
		if discount > 0 {

			var _discstr = "            " + float2str(discount)
			_discstr = "=" + _discstr[len(_discstr)-12:]

			var __discstr = encode866("    скидка                                                            ")

			__discstr = __discstr[0:48-len(_discstr)] + _discstr

			p.Write(__discstr)

			total_discount += discount
		}

		total += sum
	}

	total = total - total_discount

	if ext_total > 0 {
		total = ext_total
	}

	//Итоги
	p.Write(encode866("------------------------------------------------")) //48
	p.Linefeed()

	p.SetFontSize(2, 2)
	p.SetAlign("left")
	floattotal := "                    " + float2str(total)
	totalstr := "ИТОГ" + floattotal[len(floattotal)-20:]
	p.Write(encode866(totalstr))
	p.Linefeed()

	p.SetFontSize(1, 1)
	p.SetAlign("left")
	p.Write(time.Now().Format("02.01.2006 15:04:05"))
	p.Linefeed()
	p.Write(encode866("================================================")) //48
	p.Linefeed()

	p.SetAlign("center")
	p.SetFontSize(2, 2)
	p.Write(encode866("СПАСИБО"))
	p.Linefeed()
	p.Write(encode866("ЗА ПОКУПКУ!"))
	p.Linefeed()
	p.SetFontSize(1, 1)
	p.Write(encode866("------------------------------------------------"))
	p.Linefeed()
	p.SetAlign("center")
	p.Write(encode866(phone))
	p.Linefeed()
	p.Write(encode866(web))
	p.Linefeed()
	p.Formfeed()

	p.Cut()

	if openCashDrawer == true {
		p.OpenCashDrawer()
	}

	w.Flush()
}

func encode866(str string) string {
	r, err := charmap.CodePage866.NewEncoder().String(str)
	if err == nil {
		return r
	} else {
		return "Encode error"
	}

}

func str2float(str string) float64 {
	flt, err := strconv.ParseFloat(str, 32)
	if err != nil {

		return 0.00
	}
	return flt
}

func float2str(flt float64) string {
	return strconv.FormatFloat(flt, 'f', 2, 32)
}
